package com.github.models

import java.net.URI

case class Repository(id: Int, name: String, full_name: String, contributors_url: String)

case class Repositories(repos: List[Repository])


case class Contributor(login: String, id: Int, contributions: Int)


case class Contributors(contributors: List[Contributor])


case class ErrorMessage(message: String)


case class ErrorResponse(statusCode: Int, error: String, uri: URI)


case class FinalOutput(repoName: String, itsContributors: List[Contributor])

