package com.github.models

import play.api.libs.json._

import scala.Console.{GREEN, RED, RESET}

package object github {

  implicit val repoReads = Json.reads[Repository]

  implicit val reposReads = Json.valueReads[Repositories]

  implicit val contributorReads = Json.reads[Contributor]

  implicit val contributorsReads = Json.valueReads[Contributors]

  implicit val errorMessageFormat = Json.format[ErrorMessage]

  implicit val errorResponseFormat = Json.format[ErrorResponse]

  trait Output {
    def printInGreen(prefix: String, message: String) = print(s"${prefix} ${RESET}${GREEN} $message ${RESET}")

    def printGenericError(prefix: String, message: String) = print(s"${prefix} ${RESET}${RED} $message ${RESET}")

    def printError(error: ErrorResponse) = print(s"${RESET}${RED} Something went wrong: ${RESET} \n ${Json.prettyPrint(Json.toJson(error))}")

    def print(message: String) = println(message)
  }

}
