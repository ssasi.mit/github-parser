package com.github

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.github.client.GitHubClient
import com.github.models.github.Output
import com.github.utils.CaffeineHttpCache
import play.api.libs.ws.ahc.StandaloneAhcWSClient
import play.api.libs.ws.ahc.cache.AhcHttpCache

import scala.concurrent.ExecutionContext.Implicits._
import scala.util.{Failure, Success}


/**
  * Main class that receives command line argument.
  */
object Main extends App with Output {

  if (args.length > 0) {
    val username = args(0)
    val apiToken = sys.env("GITHUB_TOKEN")
    // Create Akka system for thread and streaming management
    implicit val actorSystem = ActorSystem("GitHubParser-system")
    val cache = new CaffeineHttpCache()
    val GITHUB_API_BASE_URI = "https://api.github.com/users"

    implicit val materializer = Materializer.matFromSystem
    implicit val wsClient = StandaloneAhcWSClient(httpCache = Some(new AhcHttpCache(cache)))
    // callback
    actorSystem.registerOnTermination {
      print(s"-- Terminating ${actorSystem} --")
      wsClient.close()
      Runtime.getRuntime().halt(0)
    }

    val client = new GitHubClient(GITHUB_API_BASE_URI, apiToken)

    client.fetchRepositories(username).onComplete {
      case Success(res) => {
        res.fold(printError, r => client.printGitHubInfo(username, r))
        closeClient(wsClient)
      }
      case Failure(e) => {
        printGenericError(">>", e.getMessage)
        closeClient(wsClient)
      }
    }

  } else {
    printGenericError(">>", "Please enter the GitHub username followed by run command: run <username>!")
  }

  def closeClient(wsClient: StandaloneAhcWSClient)(
    implicit system: ActorSystem): Unit = {
    wsClient.close()
    system.terminate()
  }

}

