package com.github.client

import akka.actor.ActorSystem
import akka.stream.Materializer
import cats.data.EitherT
import cats.implicits._
import com.github.models._
import com.github.models.github._
import play.api.libs.json.{JsValue, Reads}
import play.api.libs.ws.JsonBodyReadables._
import play.api.libs.ws.StandaloneWSRequest
import play.api.libs.ws.ahc.StandaloneAhcWSClient

import scala.Console._
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.concurrent.duration.{Duration, SECONDS}
import scala.util.Try


/**
  * Creates client to fetch repositories and its contributors from GitHub.
  */
class GitHubClient(baseUrl: String, apiToken: String)(implicit val actorSystem: ActorSystem,
                                                      implicit val materializer: Materializer,
                                                      implicit val wsClient: StandaloneAhcWSClient) extends Output {


  private def repositoryResponseHandler(response: StandaloneWSRequest#Response): Either[ErrorResponse, Repositories] = {
    response.status match {
      case code => Left(ErrorResponse(code, response.statusText, response.uri))
    }
  }

  private def contributorResponseHandler(response: StandaloneWSRequest#Response): Either[ErrorResponse, Contributors] = {
    response.status match {
      case 204 => Right(Contributors(List.empty))
      case 404 => Right(Contributors(List.empty))
      case code => Left(ErrorResponse(code, response.statusText, response.uri))
    }
  }

  def fetchRepositories(username: String): Future[Either[ErrorResponse, Seq[FinalOutput]]] = {
    val x = (for {
      repos <- EitherT(callRepos(username))
      contributors <- EitherT(callContributors(repos))
    } yield contributors).value

    x
  }


  def printGitHubInfo(username: String, res: Seq[FinalOutput]) = {
    printRepoFoundMessage(username, res.size)
    res.foreach(l => {
      printContributions(l.repoName, l.itsContributors)
    })
  }

  private def printRepoFoundMessage(username: String, repoSize: Int) = {
    printInGreen(">>", "----------------------------------------------------------")
    println(s">> ${RESET}${GREEN}${repoSize}${RESET} Repositories found for the username: ${RESET}${UNDERLINED}${username}${RESET}")
    printInGreen(">>", "----------------------------------------------------------")
  }

  /**
    * Prints the contributors list in a table along with no. of contributions.
    *
    * @param repoName
    * @param sortedContributors
    */
  private def printContributions(repoName: String, sortedContributors: List[Contributor]) = {
    sortedContributors.headOption.map { cont =>
      println(s">>>>>> Top Contributor for the repo '${repoName}' is ${RESET}${UNDERLINED}${cont.login}${RESET}")
      printInGreen(">>>>>>", "|-------------------------------------------|-----------------------|")
      printInGreen(">>>>>>", "|       Contributor name:                   |  No. of contributions |")
      printInGreen(">>>>>>", "|-------------------------------------------|-----------------------|")
    }
    sortedContributors
      .foreach(contributor => println(s">>>>>> ${RESET}${GREEN} |${RESET}       ${contributor.login}                " +
        s"${RESET}${GREEN}|${RESET}  ${contributor.contributions}        ${RESET}${GREEN}|${RESET}"))
    sortedContributors.headOption.map(_ =>
      printInGreen(">>>>>>", "|-------------------------------------------|-----------------------|")
    )
  }

  /**
    *
    * @param url - url to GET
    * @tparam A - type of Success response to bind
    * @return Future[Either[ErrorResponse, A]]
    **/
  private def call[A: Reads](url: String, handler: StandaloneWSRequest#Response => Either[ErrorResponse, A]): Future[Either[ErrorResponse, A]] = {
    createRequest(url)
      .get()
      .map { response =>
        if (response.status == 200)
          Try {
            (response.body[JsValue].as[A])
          }.toEither.left.map(e => ErrorResponse(1001, s"Parse failed ${e.getMessage}", response.uri))
        else {
          handler(response)
        }
      }
  }

  /**
    * Creates a StandaloneAhcWSRequest with/without OAuth token based on the presence of
    * env variables like
    * - GITHUB_TOKEN
    *
    * @param url to GET
    * @return StandaloneAhcWSRequest
    */
  private def createRequest(url: String): StandaloneWSRequest = {
    (if (!apiToken.isEmpty) {
      wsClient.url(url)
        .addHttpHeaders("Authorization" -> s"${apiToken} OAUTH-TOKEN")
    }
    else
      wsClient.url(url)
      ).withRequestTimeout(Duration(5, SECONDS))
  }

  /**
    * calls the repos endpoint and returns the Future with Contributors
    *
    * @param username
    * @return Future[Either[ErrorResponse, Repositories]]
    **/
  private def callRepos(username: String): Future[Either[ErrorResponse, Repositories]] = {
    call[Repositories](s"$baseUrl/${username}/repos", repositoryResponseHandler)
  }

  /**
    * calls the contributors endpoint and returns the Future with Contributors
    *
    * @param repositories
    * @return Future[Either[ErrorResponse, Seq[FinalOutput]]]
    **/
  private def callContributors(repositories: Repositories): Future[Either[ErrorResponse, Seq[FinalOutput]]] = {
    Future.sequence(repositories.repos.map { repo =>
      call[Contributors](repo.contributors_url, contributorResponseHandler).map(_.map(contrs => FinalOutput(repo.name, contrs.contributors)))
    }).map(t => flattenEither(t))
  }


  def flattenEither[A, B](x: Seq[Either[A, B]]): Either[A, Seq[B]] =
    (x foldRight (Right(Nil): Either[A, Seq[B]])) { (e, acc) =>
      for (xs <- acc.right; x <- e.right) yield (x +: xs)
    }

}