package com.github.utils

import java.util.concurrent.TimeUnit

import com.github.benmanes.caffeine.cache.{Caffeine, Ticker}
import play.api.libs.ws.ahc.cache.{Cache, EffectiveURIKey, ResponseEntry}

import scala.concurrent.Future

/**
  * Caffeine implementation of Caching that expires entries in 1 DAY.
  */
class CaffeineHttpCache extends Cache {

  val underlying = Caffeine.newBuilder()
    .ticker(Ticker.systemTicker())
    .expireAfterWrite(1, TimeUnit.DAYS)
    .build[EffectiveURIKey, ResponseEntry]()

  override def put(key: EffectiveURIKey, entry: ResponseEntry) = Future.successful(underlying.put(key, entry))

  override def remove(key: EffectiveURIKey) = Future.successful(Option(underlying.invalidate(key)))

  override def get(key: EffectiveURIKey) = Future.successful(Option(underlying getIfPresent key))

  override def close(): Unit = underlying.cleanUp()

}