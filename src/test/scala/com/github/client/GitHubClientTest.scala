package com.github.client

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.github.Main.{print, printError, printGenericError}
import com.github.models.{ErrorResponse, FinalOutput}
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, DiagrammedAssertions, FlatSpec, Matchers}
import play.api.libs.ws.ahc.StandaloneAhcWSClient

import scala.Console._
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.util.{Failure, Success}

class GitHubClientTest extends FlatSpec with Matchers with BeforeAndAfterAll with DiagrammedAssertions {

  val port = 8080
  val host = "localhost"
  val url = s"http://localhost:$port"
  val wireMockServer = new WireMockServer(wireMockConfig().port(port))
  var username = "sasikumar-swaminathan"

  implicit val actorSystem = ActorSystem("GitHubParser-Test-system")
  actorSystem.registerOnTermination {
    print(s"-- Terminating ${actorSystem} --")
    Runtime.getRuntime().halt(0)
  }
  implicit val materializer = Materializer.matFromSystem
  implicit val wsClient = StandaloneAhcWSClient()

  val client = new GitHubClient(s"$url/users", "DUMMY")


  override def beforeAll {
    println("Starting WireMock")
    wireMockServer.start()
    WireMock.configureFor(host, port)
  }

  override def afterAll {
    println("Stopping WireMock")
    wireMockServer.stop()
    actorSystem.terminate()
    wsClient.close()
  }


  "Fetching GitHub repos with the valid username" should "print repositories and its contributors descending sorted" in {
    val path = s"/users/${username}/repos"

    stubFor(get(urlEqualTo(path))
      .willReturn(
        aResponse()
          .withHeader("Content-Type", "application/json")
          .withBodyFile("repos.json")
          .withStatus(200))
    )

    val contributionPath = s"/repos/${username}/vertx-examples/contributors"

    stubFor(get(urlEqualTo(contributionPath))
      .willReturn(
        aResponse()
          .withHeader("Content-Type", "application/json")
          .withBodyFile("vertx-contributors.json")
          .withStatus(200))
    )

    val f: Future[Either[ErrorResponse, Seq[FinalOutput]]] = client.fetchRepositories(username)


    ScalaFutures.whenReady(f) { s =>
      f.map(_.fold(
        e => {
          printError(e)
        },
        res => {
          client.printGitHubInfo(username, res)

          //assertions
          assert(res.headOption.exists(e => e.repoName == "vertx-examples"))
          assert(res.headOption.exists(c => c.itsContributors.size == 1))
          assert(res.headOption.exists(c => c.itsContributors.head.login == "cescoffier"))
          assert(res.headOption.exists(c => c.itsContributors.head.contributions == 363))
        }
      ))
    }

    verify(1,
      getRequestedFor(urlEqualTo(path)))

    verify(1,
      getRequestedFor(urlEqualTo(contributionPath)))
  }


  "Fetching GitHub repos with the invalid username" should "print '0 Repositories found'" in {
    username = "dummy"
    val path = s"/users/${username}/repos"

    stubFor(get(urlEqualTo(path))
      .willReturn(
        aResponse()
          .withHeader("Content-Type", "application/json")
          .withBodyFile("empty.json")
          .withStatus(200))
    )


    client.fetchRepositories(username).onComplete {
      case Success(res) => {
        res.fold(printError, r => client.printGitHubInfo(username, r))
      }
      case Failure(e) => {
        printGenericError(">>", e.getMessage)
      }
    }

    val expectedMsg = s">> ${RESET}${GREEN}0${RESET} Repositories found for the username: ${RESET}${UNDERLINED}${username}${RESET}"
    verify(1,
      getRequestedFor(urlEqualTo(path)))

    verify(0,
      getRequestedFor(urlEqualTo("/contributors")))
  }


  "Fetching GitHub repos with the invalid contributors path" should "not print contributors" in {
    username = "invalid"
    val repo_path = s"/users/${username}/repos"
    stubFor(get(urlEqualTo(repo_path))
      .willReturn(
        aResponse()
          .withHeader("Content-Type", "application/json")
          .withBodyFile("invalid-repos.json")
          .withStatus(200))
    )

    val contributors_path = s"/repos/${username}/contributors"
    stubFor(get(urlEqualTo(contributors_path))
      .willReturn(
        aResponse()
          .withHeader("Content-Type", "application/json")
          .withStatus(204))
    )

    val f = client.fetchRepositories(username)
    ScalaFutures.whenReady(f) { s =>
      f.map(_.fold(
        e => {
          printError(e)
        },
        res => {
          client.printGitHubInfo(username, res)

          //assertions
          assert(res.headOption.exists(e => e.repoName == "invalid-repo"))
          assert(res.headOption.exists(c => c.itsContributors.size == 0))
        }
      ))
    }

    verify(1,
      getRequestedFor(urlEqualTo(repo_path)))
    verify(1,
      getRequestedFor(urlEqualTo(contributors_path)))
  }

  "Fetching GitHub repos exceeding rate-limiting" should "result in '403:Forbidden' error" in {
    username = "foo"
    val repo_path = s"/users/${username}/repos"

    stubFor(get(urlEqualTo(repo_path))
      .willReturn(
        aResponse()
          .withHeader("Content-Type", "application/json")
          .withStatus(403))
    )

    client.fetchRepositories(username).onComplete {
      case Success(res) => {
        res.fold(
          e => {
            printError(e)

            //assertions
            assert(e.statusCode == 403)
            assert(e.error == "Forbidden")
          },
          r => client.printGitHubInfo(username, r))
      }
      case Failure(e) => {
        printGenericError(">>", e.getMessage)
      }
    }

    verify(1,
      getRequestedFor(urlEqualTo(repo_path)))
  }
}