
name := "github-parser"
organization := "com.github.ssasi"
version := "1.0"

scalaVersion := "2.13.1"


lazy val playWsVersion = "2.1.2"
lazy val catsCoreVersion = "2.0.0"
lazy val scalaTestVersion = "3.0.8"
lazy val caffeineVersion = "2.5.6"
lazy val wireMockVersion = "1.33"


libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsCoreVersion,
  "com.typesafe.play" %% "play-ahc-ws-standalone" % playWsVersion,
  "com.typesafe.play" %% "play-ws-standalone-json" % playWsVersion,
  "com.github.ben-manes.caffeine" % "caffeine" % caffeineVersion,
  "com.github.tomakehurst" % "wiremock" % wireMockVersion % Test,
  "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)